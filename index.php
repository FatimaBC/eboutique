<?php 
session_start();
if(isset($_GET['login']))
{
	header("Location:Controller/index.php?ctrl=user&action=index");
}
elseif(isset($_GET['register']))
{
	header("Location:View/subscribe.php");
}
elseif(isset($_GET['logout']))
{
	header("Location:Controller/index.php?ctrl=user&action=logout");
}
elseif (isset($_GET['product']) && isset($_GET['id']))
{
	header("Location:Controller/index.php?ctrl=product&action=view&param=".$_GET['id']);
}
elseif (isset($_GET['basket']) && isset($_GET['id']))
{
	header("Location:Controller/index.php?ctrl=product&action=addBasket&param=".$_GET['id']);
}
elseif (isset($_GET['basket']))
{
	header("Location:Controller/index.php?ctrl=product&action=viewBasket");
}
elseif (isset($_GET['basketremove']) && isset($_GET['id']))
{
	header("Location:Controller/index.php?ctrl=product&action=removeBasket&param=".$_GET['id']);
}
elseif (isset($_GET['order']))
{
	header("Location:Controller/index.php?ctrl=product&action=orderBasket");
}
elseif (isset($_GET['category']) && isset($_GET['idcat']))
{
	header("Location:Controller/index.php?ctrl=category&action=index&param=".$_GET['idcat']);
}
else{
	header("Location:Controller/index.php?ctrl=home&action=index");
}
?>