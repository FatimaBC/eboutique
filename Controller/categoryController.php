<?php
require('../Model/categoryRepository.php');
require('../Model/productRepository.php');
class categoryController{
	private $cr;
	private $pr;
	public function __construct()
	{
		$this->cr=new categoryRepository();
		$this->pr=new productRepository();
	}
	public function index($id)
	{
		$page="category";
		$title=$this->cr->getCategoryNameById($id);
		$products=$this->cr->getProductsByCatId($id);
		$categories=$this->cr->findAllNames();
		$news=$this->pr->findAllByIsNew();
		$home=$this->pr->findAllByIsHome();
		require_once '../View/template.php';	
	}
}