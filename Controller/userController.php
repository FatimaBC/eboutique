<?php
require('../Model/User.php');
require('../Model/categoryRepository.php');
class userController{
	private $db;
	private $user;
	private $cr;
	public function __construct()
	{
		$this->user=new User($this->db);
		$this->cr=new categoryRepository();
		
	}
	public function index(){
		$page="login";
		$title="Login";
		$categories=$this->cr->findAllNames();
		require_once '../View/template.php';
	}
	public function login()
	{
		if( isset($_POST['email'] ) && isset($_POST['password']) && isset($_POST['submit']))
		{
			$email=$_POST['email'];
			$password=$_POST['password'];
			$res=$this->user->login($email,$password);
			if($res)
			{	
				header("Location:../index.php");
			}
			else{
				header("Location:../index.php?login");
			}
		}
	}
	public function logout()
	{
		session_start();
		// On �crase le tableau de session
		$_SESSION = array();
		// On d�truit la session
		session_destroy();
		// On redirige le visiteur vers la page d'accueil
		header ('Location: ../index.php');
	}
}