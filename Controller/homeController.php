<?php
require('../Model/categoryRepository.php');
require('../Model/productRepository.php');
class homeController{
	private $cr;
	private $pr;
	public function __construct()
	{
		$this->cr=new categoryRepository();
		$this->pr=new productRepository();
	}
	public function index()
	{
		$page="home";
		$title="Home";
		$categories=$this->cr->findAllNames();
		$news=$this->pr->findAllByIsNew();
		$home=$this->pr->findAllByIsHome();
		require_once '../View/template.php';
		
	}
}