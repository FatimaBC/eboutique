<?php
require('../Model/categoryRepository.php');
require('../Model/productRepository.php');
require('../Model/orderRepository.php');
class productController{
	private $cr;
	private $pr;
	private $or;
	public function __construct()
	{
		$this->cr=new categoryRepository();
		$this->pr=new productRepository();
		$this->or=new orderRepository();
	}
	public function view($id)
	{
		$id=intval($id);
		$categories=$this->cr->findAllNames();
		$product=$this->pr->findById($id);
		$page="product";
		$title=$product['productName'];
		require_once '../View/template.php';
	}
	public function addBasket($id)
	{
	if(isset($_SESSION['uid']))
		{
			$id=intval($id);
			$product=$this->pr->findById($id);
			//$_SESSION['basket'][]=$product;
			$_SESSION['basket'][]=$id;
			$categories=$this->cr->findAllNames();
			$page="product";
			$title=$product['productName'];
			require_once '../View/template.php';
		}
		else {
			header('Location:../index.php?login');
		}
		
	}
	public function removeBasket($id)
	{
		if(isset($_SESSION['uid']))
		{
			$id=intval($id);
			$product=$this->pr->findById($id);
			unset($_SESSION['basket'],$_SESSION['basket'][$id]);
			$categories=$this->cr->findAllNames();
			$page="basket";
			$title="Your basket";
			require_once '../View/template.php';
		}
		else {
			header('Location:../index.php?login');
		}
	}
	public function viewBasket()
	{
		if(isset($_SESSION['uid']))
		{
			$basket=array_count_values($_SESSION['basket']);
			$categories=$this->cr->findAllNames();
			$page="basket";
			$title="Your basket";
			var_dump($_SESSION['basket']);
			var_dump($basket);
				
			require_once '../View/template.php';
		}
		else {
			header('Location:../index.php?login');
		}
	}
	public function orderBasket()
	{
		if(isset($_SESSION['uid']))
		{
			$_SESSION['basket']=array_count_values($_SESSION['basket']);
			$amount=$this->or->getAmount($_SESSION['basket']);
			$date=date('Y-m-d H:i:s');
			$email=$_SESSION['uid'];
			$list=$_SESSION['basket'];
			$this->or->saveOrder($amount,$date,$email,$list);
			$categories=$this->cr->findAllNames();
			$page="basket";
			$title="Your basket";
			$_SESSION['basket']=array();
			require_once '../View/template.php';
		}
		else {
			header('Location:../index.php?login');
		}
	}
}