<?php 
class Connection
{
	private $host;
	private $dbname;
	private $username;
	private $password;
	
    public function __construct(){
	// les identifiants de connexion sont stock�es ici comme attributs de la classe connection
        $this->host = 'localhost';
        $this->dbname = 'eboutique';
        $this->username = 'root';
        $this->password = '';
    }
    public function get_connection(){ //Connexion � la base
    	try
    	{
        	return new PDO('mysql:host=' . $this->host . ';dbname=' . $this->dbname, $this->username, $this->password);
    	}
    	catch (Exception $e)
    	{
    		die('Erreur : ' . $e->getMessage());
    	}
    }
}
?>