<?php
class orderRepository
{
	private $id;
	private $db;
	public function __construct()
	{
		$this->db=new Connection();
		$this->db=$this->db->get_connection();
		
	}
	public function getAmount($list)
	{
		$amount=null;
		foreach ($list as $l)
		{
			$amount+=$l['productPrice'];
		}
		return $amount;
	}
	public function saveOrder($amount,$date,$email,$list)
	{
		$this->id++;
		$rep=$this->db->prepare("INSERT INTO eboutique.order VALUES(:id, :amount, :date, :email)");
		$rep->execute(array('id'=>$this->id,'amount'=>$amount,'date'=>$date,'email'=>$email));
		foreach($list as $l)
		{			
			$req=$this->db->prepare("INSERT INTO product_has_order VALUES(:idp, :ido)");
			$req->execute(array('idp'=>$l['idProduct'], 'ido'=>$this->id));
		}
	}
}