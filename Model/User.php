<?php
require('connection.php');

class User {
	private $id;
	private $password;
	private $email;
	private $firstname;
	private $lastname;
	private $sex;
	private $db;

	public function __construct(){
	// Le constructeur re�oit en argument l'identifiant de la connexion
	// et le transfert � l'attribut $db
		$this->db=new Connection();
	    $this->db=    $this->db->get_connection();
    }
    public function login($username,$password){
    	$reponse = $this->db->prepare('SELECT * FROM user WHERE email = :username AND password = :password' );
    	$reponse->execute(array('username' => $username, 'password' => $password));
    	$nb=$reponse->rowCount();
    	$reponse = $reponse->fetch();
    	if($nb>=1)
    	{
    		$_SESSION['nom']=$reponse['lastName'];
    		$_SESSION['prenom']=$reponse['firstName'];
    		$_SESSION['uid']=$reponse['email']; // Storing user session value
    		$_SESSION['password']=$reponse['password']; // Storing user session value
    		$_SESSION['basket']=array();
			return true;
    	}
    	else{
    		return false;
    	}
    }
}
?>