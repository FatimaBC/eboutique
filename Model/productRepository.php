<?php
require('connection.php');
class productRepository
{
	private $db;
	public function __construct()
	{
		$this->db=new Connection();
		$this->db=$this->db->get_connection();
	}
	public function findAll(){
		$products=array();
		$reponse= $this->db->prepare('SELECT * FROM product' );
    	$reponse->execute();
    	while($donnees=$reponse->fetch())
    	{
    		array_push($products,$donnees);
    	}
    	return $products;
	}
	public function findAllByIsNew()
	{
		$news=array();
		$reponse= $this->db->prepare('SELECT * FROM product WHERE productIsNew = 1');
		$reponse->execute();
		while($donnees=$reponse->fetch())
		{
			array_push($news,$donnees);
		}
		return $news;
	}
	public function findAllByIsHome()
	{
		$home=array();
		$reponse= $this->db->prepare('SELECT * FROM product WHERE productIsHome = 1');
		$reponse->execute();
		while($donnees=$reponse->fetch())
		{
			array_push($home,$donnees);
		}
		return $home;
	}
	public function findById($id)
	{
		$product=null;
		$reponse= $this->db->prepare('SELECT * FROM product WHERE idProduct =:id');
		$reponse->execute(array('id'=>$id));
		$product=$reponse->fetch();
		return $product;
	}
}