<?php
class categoryRepository
{
	private $db;
	public function __construct()
	{
		$this->db=new Connection();
		$this->db=$this->db->get_connection();
	}
	public function findAll(){
		$categories=array();
		$reponse= $this->db->prepare('SELECT * FROM category' );
    	$reponse->execute();
    	while($donnees=$reponse->fetch())
    	{
    		array_push($categories,$donnees);
    	}
    	return $categories;
	}
	public function findAllNames()
	{
		$categories=array();
		$reponse= $this->db->prepare('SELECT * FROM category' );
		$reponse->execute();
		while($donnees=$reponse->fetch())
		{
			array_push($categories,$donnees['categoryName']);
		}
		return $categories;
	}
	public function getCategoryNameById($id)
	{
		$reponse= $this->db->prepare('SELECT categoryName FROM category WHERE idCategory =:id' );
		$reponse->execute(array('id'=>$id));
		$donnees=$reponse->fetch();
		return $donnees['categoryName'];
	}
	public function getProductsByCatId($id)
	{
		$products=array();
		$reponse= $this->db->prepare('SELECT product.idProduct, productName, productPrice, productDescription, productImage FROM product_has_category,product 
				 WHERE product.idProduct=product_has_category.idProduct 
				AND idCategory = :id ' );
		$reponse->execute(array('id'=>$id));
		while($donnees=$reponse->fetch())
		{
			array_push($products,$donnees);
		}
		return $products;
	}
}