<div class="container">
	<div class="row">
		<div class="photo col-md-6">
			<img src="../Assets/img/products/<?=$product['productImage']?>">
		</div>
		<div class="description col-md-6">
			<?=$product['productName']?>
			<?=$product['productPrice']?>
			<?=$product['productDescription']?>
			<?=$product['productQuantity']?>
			<?php if($product['productQuantity']>0)
			{?>
				<a href="../index.php?basket&id=<?=$product['idProduct']?>" class="btn btn-outline-default waves-effect"> Buy it</a>
			<?php }
			else{
			echo "Out of stock.";
			}?>
		</div>
	</div>
</div>