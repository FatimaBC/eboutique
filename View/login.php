<?php
$title="Login";

?>

<div class="row">
<div class="card col-md-6 col-md-offset-3">
    <form method="post" action="../Controller/index.php?ctrl=user&action=login">
	    <div class="card-block">
	      <!--Header-->
	        <h1>Login</h1>
	    	  <!--Body-->
	        <div class="md-form">
	            <i class="fa fa-envelope prefix"></i>
	            <input type="email" name="email" id="form2" class="form-control" placeholder="Your email">
	        </div>
	
	        <div class="md-form">
	            <i class="fa fa-lock prefix"></i>
	            <input type="password" name="password" id="form4" class="form-control"  placeholder="Your password">
	        </div>
	
	        <div class="text-center">
	            <input type="submit" name="submit" class="btn btn-deep-purple" value="Login">
	        </div>
	    	 <!--Footer-->
		    <div class="modal-footer">
		        <div class="options">
		            <p>Not a member? <a href="#">Sign Up</a></p>
		            <p>Forgot <a href="#">Password?</a></p>
		        </div>
		    </div>
		</div>
	</form>
</div>
</div>

<?php 
?>