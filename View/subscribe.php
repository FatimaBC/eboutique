<?php
$title="Subscribe";
include('header.inc.php');
?>

<div class="row">
	<div class="card col-md-6 col-md-offset-3">
	
	
	
<!--Form with header-->
    <div class="card-block">

        <!--Header-->
            <h1>Register:</h1>

        <!--Body-->
        <div class="md-form">
            <i class="fa fa-user prefix"></i>
            <input type="text" id="form3" class="form-control"  placeholder="Your name">
        </div>
        <div class="md-form">
            <i class="fa fa-envelope prefix"></i>
            <input type="text" id="form2" class="form-control"  placeholder="Your email">
        </div>

        <div class="md-form">
            <i class="fa fa-lock prefix"></i>
            <input type="password" id="form4" class="form-control"  placeholder="Your password">
        </div>

        <div class="text-center">
            <button class="btn btn-indigo">Sign up</button>
            <hr>
            <fieldset class="form-group">
                <input type="checkbox" id="checkbox1">
                <label for="checkbox1">Subscribe me to the newsletter</label>
            </fieldset>
        </div>

    </div>
<!--/Form with header-->
	
	
	
	</div>
</div>

<?php 
include('footer.inc.php');
?>