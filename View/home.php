<div class="container">
	<div class="row" style="margin:0">
		<div class="myNav col-md-2" >
			<h1>Categories</h1>
			<!--Panel-->
			<div class="widget-wrapper">
	        	<div class="list-group">
	        	 <?php foreach($categories as $categorie )
		            { ?>
		            <a href="#" class="list-group-item"><?=$categorie?></a>
		      <?php }?>
	             </div>
	        </div>
			<!--/.Panel-->
		</div>
		<div class="news col-md-10 ">
			<div class="divider-new">                                          
				<h1>
					What's new ?
				</h1>
			</div>
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			  </ol>	
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			   <?php 
			  for($i=0;$i<sizeof($news);$i++)
			  {?>
			  	<div class="item <?php if($i==0){echo'active';}?>">
			   		<img src="../Assets/img/products/<?=$news[$i]['productImage']?>" alt="...">
			   		<div class="carousel-caption"  style="color:black; font-size: 20pt;">
			       		<?=$news[$i]['productName'].' '.$news[$i]['productPrice']?><br/><a href="../index.php?product&id=<?=$news[$i]['idProduct']?>" class="btn btn-outline-default waves-effect">View it!</a>
			       </div>
			   </div>
			 <?php } ?> 
			  </div>	
			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="divider-new">                                          		
			<h1>Collections</h1>
		</div>
		<div class="collections row text-center">
			<div class="collection-item cat1 col-md-5">
				<div><a>Catégorie 1 </a><i class="fa fa-angle-right" style="float:right" aria-hidden="true"></i></div>
			</div>
			<div class="collection-item cat2 col-md-5">
				<div><a>Catégorie 2 </a><i class="fa fa-angle-right" style="float:right" aria-hidden="true"></i></div>
			</div>
			<div class="collection-item cat3 col-md-5">
				<div><a>Catégorie 3</a> <i class="fa fa-angle-right" style="float:right" aria-hidden="true"></i></div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="divider-new">                                          		
			<h1>Featured</h1>
		</div>
		 <div class="row">
		<?php 
		foreach($home as $h)
		{?>
			 <div class="col-md-6">
                <!--Card-->
             	<div class="card  wow fadeIn" data-wow-delay="0.2s">
					<!--Card image-->
                     <div class="view overlay hm-white-slight">
                     	<img src="../Assets/img/products/<?= $h['productImage']?>" class="img-fluid" alt="">
                     </div>
                      <!--/.Card image-->
                      <!--Card content-->
                      <div class="card-block">
                     	<!--Title-->
                         <h4 class="card-title"><?=$h['productName']?></h4>
                         <!--Text-->
                         <p class="card-text"><?=$h['productDescription']?></p>
                         <a href="#" class="btn btn-outline-default waves-effect">Buy now for <strong><?=$h['productPrice']?></strong></a>
                     </div>
                     <!--/.Card content-->
				 </div>
                 <!--/.Card-->
           </div>
		<?php }?>
	   </div>
    </div>
</div>