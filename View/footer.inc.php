
 	<!--Footer-->
	<footer class="page-footer special-color-dark center-on-small-only">
	    <!--Footer Links-->
	    <div class="container-fluid">
	        <div class="row">
	            <!--First column-->
	            <div class="col-md-6">
	                <h5 class="title">Footer Content</h5>
	                <p>Here you can use rows and columns here to organize your footer content.</p>
	            </div>
	            <!--/.First column-->
	
	            <!--Second column-->
	            <div class="col-md-6">
	                <h5 class="title">Links</h5>
	                <ul>
	                    <li><a href="#!">Link 1</a></li>
	                    <li><a href="#!">Link 2</a></li>
	                    <li><a href="#!">Link 3</a></li>
	                    <li><a href="#!">Link 4</a></li>
	                </ul>
	            </div>
	            <!--/.Second column-->
	        </div>
	    </div>
	   
	    <div class="" style="text-align:center">
	    	<button type="button" class="btn btn-secondary">Subscribe</button>
	    </div>
	     <div class="navbar" >
			<ul class="nav navbar-nav">
	            <li><a class=" nav-item btn btn-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	            <li><a class="nav-item btn btn-info"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>	            
	            <li><a class="nav-item btn btn-danger"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
	    	</ul>
	    </div>
	    <!--/.Footer Links-->
	    <!--Copyright-->
	    <div class="footer-copyright">
	        <div class="container-fluid">
	             2017 <a href="https://fatimaboudieb.wordpress.com"> Fatima Chabi </a>
	
	        </div>
	    </div>
	    <!--/.Copyright-->
	</footer>
	<!--/.Footer-->
</body>
</html>