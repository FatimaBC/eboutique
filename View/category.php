<div class="container">
<ul class="row">
<?php foreach($products as $product)
{?>
	<li class="col-md-3">
		<!--Card-->
		<div class="card text-center">
		    <!--Card image-->
		    <img class="img-fluid" style="height:200px;" src="../Assets/img/products/<?=$product['productImage']?>" alt="Card image cap">
		    <!--/.Card image-->
		    <!--Card content-->
		    <div class="card-block">
		        <!--Title-->
		        <h4 class="card-title"><?=$product['productName']?></h4>
		        <!--Text-->
		        <p class="card-text"><?=$product['productPrice'].'$ '.$product['productDescription']?></p>
		        <a href="../index.php?product&id=<?=$product['idProduct']?>" class="btn btn-primary">More</a>
		    </div>
		    <!--/.Card content-->
		</div>
		<!--/.Card-->
	</li>
<?php }?>
</ul>
</div>