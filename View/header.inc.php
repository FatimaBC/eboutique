<!doctype html>
<html>
<head>
	<title><?=$title?></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../Assets/css/general.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/css/mdb.min.css">	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.0/js/mdb.min.js"></script>
	<script src="../Assets/js/zoomify.js"></script>
</head>
<body>
	<div class="header">
		<!--Navbar-->
		<nav class="navbar navbar-dark special-color-dark">
		    <div class="container">
		        <!--Navbar Brand-->
	            <a class="navbar-brand" href="/eboutique/index.php">eMode</a>
	            <!--Links-->
	            <ul class="nav navbar-nav">
	      <?php for($i=1;$i<sizeof($categories);$i++ )
	            { ?>
	            <li class="nav-item">
	                    <a class="nav-link" href="/eboutique/index.php?category&idcat=<?=$i?>"><?=$categories[$i-1]?></a>
	                </li>
	      <?php }?>
	            </ul>
	            <div class="" style="float:right;">
		            <ul class="nav navbar-nav">
		            <?php if(isset($_SESSION['nom']))
		            {
		            ?>
		            	<li class="nav-item">
		                    <a class="nav-link" href="/eboutique/index.php?logout"><span class="glyphicon glyphicon-log-in"></span> <?=$_SESSION['nom']?></a>
		                </li>
		            <?php }
		            else{?>
		                <li class="nav-item">
		                    <a class="nav-link" href="/eboutique/index.php?login"><span class="glyphicon glyphicon-log-in"></span> Login</a>
		                </li>
		                <li class="nav-item">
		                    <a class="nav-link" href="/eboutique/index.php?register"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a>
		                </li>
		                <?php }?>
		                <li class="nav-item">
		                    <a class="nav-link" href="/eboutique/index.php?basket"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Basket
		                    <?php if(isset($_SESSION['basket'])){?> <span class="badge red"><?=sizeof($_SESSION['basket'])?></span><?php }?>
		                    </a>
		                </li>
		            </ul>
	            </div>
		    </div>
		</nav>
	</div> 
